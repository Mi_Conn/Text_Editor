/* Michael Villalobos
 * Helper Class For Main Program Class: Allows for the capability of editing the contents of individual lines
 * February 22, 2017
 */

package project1;
import java.util.*;
public class Lines {
	
	private static char[] lineChars;
	private static char[] text;
	private static char[] newString;
	private static char[] stringBuffer;
	private static int pos;
	private static Scanner scan = new Scanner(System.in);
	
	// METHOD USED TO COPY A SUBSTRING INTO
	// A LINE BUFFER
	public static void test() {
		for(int i = 0; i < stringBuffer.length; i++) {
			System.out.print(stringBuffer[i]);
		}
	}
	
	// METHOD USED TO INDICATE SUBSTRINGS THAT ARE SELECTED TO BE 
	// COPIED, CUT, OR DELETED
	public static void displayArrows(int from, int to) {
		System.out.println("");
		for(int i = 0; i < from; i++)
			System.out.print(" ");
		for(int k = from; k <= to; ++k)
			System.out.print("^");
	}
	
	// METHOD USED TO PASTE A SUBSTRING FROM THE BUFFER
	// INTO A POSITION INDICATED BY THE USER
	public static void pasteString() {
		System.out.print("Insert after position: ");
		String position = scan.nextLine();
		int pasteAfter = Integer.parseInt(position);
		int positionP = 0;;
		int index;
		newString = new char[stringBuffer.length + lineChars.length];
		for(int i = 0; i < pasteAfter; i++) {
			newString[i] = lineChars[i];
			positionP = i;
		}
		positionP++;
		index = positionP;
		for(int j = 0; j < stringBuffer.length; j++) {
			newString[positionP] = stringBuffer[j];
			positionP++;
		}
		for(int k = index; k < lineChars.length; k++) {
			newString[positionP] = lineChars[k];
			positionP++;
		}
		lineChars = new char[newString.length];
		lineChars = newString;
	}
	
	public static void cut(int beginIndex, int endIndex) {
		// COPY SECTION
		int size = (endIndex - beginIndex);
		stringBuffer = new char[size+1];
		int index = 0;
		// COPY THE CONTENTS OF THE SUBSTRING SPECIFIED
		for(int i = beginIndex; i <= endIndex; i++) {
			stringBuffer[index] = lineChars[i];
			index++;
		}
		System.out.println("Cut:");
		printArray();
		displayArrows(beginIndex, endIndex);
		// DELETE PORTION OF THE CUT OPTION FROM THE SUBMENU
		System.out.print("\ny/n: ");
		String reply = scan.nextLine();
		if(reply.equals("y")) {
			int Size = endIndex - beginIndex;
			newString = new char[lineChars.length - Size];
			int pos = 0;
			for(int i = 0; i < beginIndex; i++) {
				newString[i] = lineChars[i];
				pos = i;
			}
			pos++;
			for(int k = endIndex; k < lineChars.length; k++) {
				newString[pos] = lineChars[k];
				pos++;
			}
			lineChars = newString;
		}
	}
		
	// METHOD USED TO ADD A SUBSTRING INTO A STRING
	public static String substring(int beginIndex)
	{
		int pos2;
		Scanner newScan = new Scanner(System.in);
		System.out.print("Text: ");
		String newText = newScan.nextLine();
		newCharArray(newText);
		newString = new char[lineChars.length + text.length];
		
		// COPY THE CONTENTS OF THE LINE INTO THE NEW STRING ARRAY
		// UNTIL YOU REACH THE POINT OF INSERTION
		for(int i = 0; i < beginIndex; i++) {
			newString[i] = lineChars[i];
			pos = i;
		}
		pos++;
		pos2 = pos;
		
		// ADD THE NEW TEXT INTO THE NEW STRING ARRAY
		// FROM THE POINT OF INSERTION
		for(int j = 0; j < text.length; j++) {
			newString[pos] = text[j];
			pos++;
		}
		
		// ADD THE REST OF THE ORIGINA LINE INTO THE NEW STRING ARRAY
		for(int k = pos2; k < lineChars.length; k ++) {
			newString[pos] = lineChars[k];
			pos++;
		}
		lineChars = newString;
		return "";
	}
	
	// METHOD USED TO COPY SUBSTRING INTO A STRING BUFFER
	public static String substring(int beginIndex, int endIndex)
	{
		int size = (endIndex - beginIndex);
		stringBuffer = new char[size+1];
		int index = 0;
		// COPY THE CONTENTS OF THE SUBSTRING SPECIFIED
		for(int i = beginIndex; i <= endIndex; i++) {
			stringBuffer[index] = lineChars[i];
			index++;
		}
		System.out.println("Copied:");
		printArray();
		displayArrows(beginIndex, endIndex);
		return "";
	}

	// METHOD TO DELTE A SUBSTRING FROM THE LINE
	public static void deleteSubstring(int begin, int end) {	
		System.out.println("Delete:");
		printArray();
		displayArrows(begin, end);
		System.out.print("\ny/n: ");
		String reply = scan.nextLine();
		if(reply.equals("y")) {
			int size = end - begin;
			newString = new char[lineChars.length - size];
			int pos = 0;
			for(int i = 0; i < begin; i++) {
				newString[i] = lineChars[i];
				pos = i;
			}
			pos++;
			for(int k = end; k < lineChars.length; k++) {
				newString[pos] = lineChars[k];
				pos++;
			}
			lineChars = newString;
		}		
	}
	
	// CONVERTS A STRING INTO AN ARRAY OF CHAR
	// USED WHEN ADDING A SUBSTRING INTO A LINE
	// CALLED FROM substring(int beginIndex)
	public static void newCharArray(String n) {	
		int lineLen = n.length();
		text = new char[lineLen];
		for(int i = 0; i < lineLen; i++) {
			text[i] = n.charAt(i);
		}
	}
	
	// CONVERT THE STRING INTO AN ARRAY OF CHAR
	public static void toCharArray(String s) {
		if(s == null)
			System.out.println("Line does not exist.");
		else {
			int lineLen = s.length();
			lineChars = new char[lineLen];
			for(int i = 0; i < lineLen; i++) {
				lineChars[i] = s.charAt(i);
			}
		}
	}
	
	// PRINT THE LINE FROM THE LINE BUFFER
	public static void printArray() {
		// IF THE LINE OF TEXT IS OF LENGTH 5 OR LESS
		if(lineChars.length <= 5) {
			for(int k = 0; k < lineChars.length; k++) {
				if(k%5 >= 1)
					System.out.print(" ");
				else
					System.out.print(k);
			}
		}
		int t = 0;
		// IF THE LENGTH OF THE TEXT IS MORE THAN LENGTH 5
		// THIS SERIES WILL PROPERLY DISPLAY THE NUMBERS ON 
		// THEIR CORRESPONDING LINE
		if(lineChars.length > 5) {
			int tmp = 0;
			int count = 0;
			while(tmp <= 5) {
				if(tmp%5 >= 1)
					System.out.print(" ");
				else
					System.out.print(tmp);
				tmp++;
			}
			while(tmp < lineChars.length) {
				if(count < 3) {
					System.out.print(" ");
					count++;
					tmp++;
				}				
				else {
					tmp++;
					count = 0;
					System.out.print(tmp);
					tmp++;
				}
			}
		}
		
		System.out.println("");

		// DISPLAY THE SYMBOLS ON THE APPROPRAITE POSITION
		for(int j= 0; j < lineChars.length; j++) {
			if(j == 0 || j == 10 || j == 20 || j == 30 || j == 40 || j == 50
					|| j == 60 || j == 70 || j == 80 || j == 90 || j == 100) {
				System.out.print("|");
			}
			else if(j == 5 || j == 15 || j == 25 || j == 35 || j == 45 || j == 55
					|| j == 65 || j == 75 || j == 85 || j == 95) {
				System.out.print("+");
			}
			else
				System.out.print("-");
		}
		System.out.println("");

		// PRINT OUT THE LINE OF TEXT
		for(int i = 0; i < lineChars.length; i++) {
			System.out.print(lineChars[i]);
		}
	}
	
	// RETURN THE STRING REPRESENTATION OF THE ARRAY OF CHARACTERS  
	public static String toString(char c) {
		return String.valueOf(lineChars);
	}
}
