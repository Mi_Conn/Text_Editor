/* Michael Villalobos
 * Main File
 * February 22, 2017
 */

package project1;

import java.util.Scanner;
import project1.Documents;

public class Proj1 {

	public static void main(String[] args) {
		mainMenu();
		Scanner scanner = new Scanner(System.in);
		String option = scanner.nextLine();
		boolean Break = false;
		String fileName;
		// MAIN LOOP FOR THE PROGRAM TO RUN
		while(option != "q" && Break != true)
		{
			// EACH CASE REPRESENTS AN OPTION FROM THE MAINMENU 
			// FOR THE USER TO CHOOSE FROM
			switch (option)
			{
				// MAINMENU
				case "m":
					mainMenu();
					option = scanner.nextLine();
					break;
				// DELETE A LINE FROM THE DOCUMENT
				case "dl":
					String Lnum;
					System.out.print("Which line will be deleted: ");
					Lnum = scanner.nextLine();
					int Number = Integer.parseInt(Lnum);
					Documents.deleteLine(Number);
					System.out.print("\n-> ");
					option = scanner.nextLine();
					break;
				// LOAD A DOCUMENTS AND DISPLAY ITS CONTENTS
				case "l":
					Documents.deleteAll();
					Documents.loadFile();
					System.out.print("\n-> ");
					option = scanner.nextLine();
					break;
				// DELETE A RANGE OF LINES FROM THE BUFFER
				case "dr":
					String Dfrom, Dto;
					System.out.print("From line: ");
					Dfrom = scanner.nextLine();
					System.out.print("To line: ");
					Dto = scanner.nextLine();
					Documents.deleteRange(Dfrom, Dto);
					System.out.print("\n-> ");
					option = scanner.nextLine();
					break;
				// SHOW ALL THE LINE CURRENTLY IN THE BUFFER
				case "sa":
					Documents.showAll();
					System.out.print("\n-> ");
					option = scanner.nextLine();
					break;
				// COPY A RANGE OF LINES FROM THE BUFFER 
				case "cr":
					String Cfrom, Cto;
					System.out.print("From line: ");
					Cfrom = scanner.nextLine();
					System.out.print("To line: ");
					Cto = scanner.nextLine();
					Documents.copyRange(Cfrom, Cto);
					System.out.print("\n-> ");
					option = scanner.nextLine();
					break;
				// SHOW A SINGLE LINE WITHIN THE BUFFER
				case "sl":
					String sLine;
					System.out.print("Which line will be displayed: ");
					sLine = scanner.nextLine();
					//int target = Integer.parseInt(sLine);
					Documents.showLine(sLine);
					System.out.print("\n-> ");
					option = scanner.nextLine();
					break;
				// PASTE A COPIED LINE INTO A LOCATION IN THE BUFFER
				case "pl":
					String Pfrom;
					System.out.print("After line: ");
					Pfrom = scanner.nextLine();
					int PAfter = Integer.parseInt(Pfrom);
					Documents.pasteLines(PAfter);
					System.out.print("\n-> ");
					option = scanner.nextLine();
					break;
				// SHOW A RANGE OF LINE IN THE BUFFER
				case "sr":
					String Sfrom, Sto;
					System.out.print("From line: ");
					Sfrom = scanner.nextLine();
					System.out.print("To line: ");
					Sto = scanner.nextLine();
					Documents.showRange(Sfrom, Sto);
					System.out.print("\n-> ");
					option = scanner.nextLine();
					break;
				// WRITE(SAVE) THE CONTENTS OF THE BUFFER TO A FILE
				case "w":
					System.out.print("Enter file name: ");
					fileName = scanner.nextLine();
					Documents.writeFile(fileName);
					System.out.print("\n-> ");
					option = scanner.nextLine();
					break;
				// ENTER A NEW LINE INTO THE BUFFER
				case "nl":
					int count = 1;
					String cond = "";
					// IF THERE ARE NO LINES IN THE BUFFER
					// CREATE A NEW SET
					if(Documents.isEmpty()) {
						do {
							System.out.print("type line? (y/n): ");
							String type = scanner.nextLine();
							if(type.equals("y")) {
								System.out.print(count+": ");
								String line = scanner.nextLine();
								Documents.newLine(line);
								count++;
							}
							else if(type.equals("n")) {
								cond = "n";
							}
						} while (!cond.equals("n"));	
					}
					// ELSE IF THE BUFFER HAS LINES IN IT
					// SELECT A LINE TO INSERT NEW LINES AFTER
					else {
						System.out.print("Insert after line number: ");
						String Lnumber = scanner.nextLine();
						int lineNumber = Integer.parseInt(Lnumber);
						System.out.println("Inserting after:");
						Documents.showLine(Lnumber);
						do {
							System.out.print("type line? (y/n): ");
							String type = scanner.nextLine();
							if(type.equals("y")) {
								System.out.print(count+": ");
								String line = scanner.nextLine();
								Documents.newLine2(line, lineNumber);
								count++;
								lineNumber++;
							}
							else if(type.equals("n")) {
								cond = "n";
							}
						} while (!cond.equals("n"));
					}				
					System.out.print("\n-> ");
					option = scanner.nextLine();
					break;
				// QUIT THE PROGRAM
				case "q":
					Break = true;
					break;
				// OPTION TO EDIT A LINE SELECTED FROM THE BUFFER
				// BY THE USER, OPENS A SUBMENU WHICH ALLOWS THE USER TO 
				// ALTER THE CONTENTS OF A LINE FROM THE LINE BUFFER
				case "el":
					String numLine;
					String option2;
					System.out.print("Line number: ");
					numLine = scanner.nextLine();
					int tmp = Integer.parseInt(numLine);
					while(tmp > Documents.stringSize()) {
						System.out.println("Line does not exist");
						System.out.print("Line number: ");
						numLine = scanner.nextLine();
						tmp = Integer.parseInt(numLine);
					}
					Lines.toCharArray(Documents.showLine(numLine));
					Lines.printArray();
					// SECONDARY PROGRAM LOOP IN WHICH THE USER IS ABLE TO 
					// MODIFY THE CONTENTS OF A LINE
					do {
						subMenu();
						option2 = scanner.nextLine();
						switch(option2) {
							// SHOW LINE SELECTED FROM THE LINE BUFFER
							case "s":
								Lines.printArray();
								break;
							// COPY SUBSTRING FROM THE STRING BUFFER
							case "c":
								System.out.print("From position: ");
								String beginPos = scanner.nextLine();
								System.out.print("To position: ");
								String toPos = scanner.nextLine();
								int from = Integer.parseInt(beginPos);
								int to = Integer.parseInt(toPos);
								Lines.substring(from, to);
								break;
							// COPY SUBSTRING INTO LINE BUFFER THEN DELETE SUBSTRING FROM THE LINE
							case "t":
								System.out.print("From position: ");
								String startCut = scanner.nextLine();
								System.out.print("To position: ");
								String endCut = scanner.nextLine();
								int sC = Integer.parseInt(startCut);
								int eC = Integer.parseInt(endCut);
								Lines.cut(sC, eC);
								break;
							// PASTE COPIED SUBSTRING INTO A POSITION IN THE LINE
							case "p":
								Lines.pasteString();
								Lines.printArray();
								break;
							// ENTER A NEW SUBSTRING INTO A POSITION IN THE LINE
							case "e":
								Lines.printArray();
								System.out.print("\nInsert at position: ");
								numLine = scanner.nextLine();
								int NL = Integer.parseInt(numLine);
								Lines.substring(NL);
								Lines.printArray();
								break;
							// DELETE A SUBSTRING FROM THE LINE
							case "d":
								System.out.print("From position: ");
								String begin = scanner.nextLine();
								System.out.print("To position: ");
								String end = scanner.nextLine();
								int beginI = Integer.parseInt(begin);
								int endI = Integer.parseInt(end);
								Lines.deleteSubstring(beginI, endI);
								Lines.printArray();
								break;
							// QUIT THE STRING EDITOR
							case "q":
								char c = ' ';
								Documents.setData(Lines.toString(c), tmp);
								break;
							default:
								System.out.println("Invalid choice, Please choose from the list.");
								System.out.print("\n-> ");
								option2 = scanner.nextLine();
								break;
						}
					} while(!option2.equals("q"));
					mainMenu();
					option = scanner.nextLine();
					break;
				// WRITE(SAVE) THE CONTENTS OF THE LINE BUFFER TO A FILE 
				// THEN QUIT THE PROGRAM
				case "wq":
					System.out.print("Enter file name: ");
					fileName = scanner.nextLine();
					Documents.writeFile(fileName);
					Break = true;
					break;
				default:
					System.out.println("Invalid input, please choose an option from the menu.");
					System.out.print("\n-> ");
					option = scanner.nextLine();
			}
		}
		scanner.close();
		System.out.println("");
		System.out.println("");
		System.out.println("Program Successfully Terminated.");
	}

	// DISPLAY THE MAINMENU OF THR PROGRAM
	public static void mainMenu()
	{
		System.out.println("*******************************************************************");
		System.out.println("|Menu:			m		Delete line:		dl|");
		System.out.println("|Load file:		l		Delete range:		dr|");
		System.out.println("|Show all:		sa		Copy range:		cr|");
		System.out.println("|Show line:		sl		Paste lines:		pl|");
		System.out.println("|Show range:		sr		Write to file:		w |");
		System.out.println("|New line:		nl		Quit:			q |");
		System.out.println("|Edit line:		el		Write and quit:		wq|");
		System.out.println("*******************************************************************");
		System.out.print("-> ");
	}
	
	// DISPLAY THE SUBMENU FOR LINE EDITING
	public static void subMenu()
	{
		System.out.println("");
		System.out.println("	Show line:			s");
		System.out.println("	Copy to string buffer:		c");
		System.out.println("	Cut:				t");
		System.out.println("	Paste from string buffer:	p");
		System.out.println("	Enter new substring:		e");
		System.out.println("	Delete substring:		d");
		System.out.println("	Quit line:			q");
		System.out.print("-> ");
	}
}
