/* Michael Villalobos
 * Helper Class For Main Program Class: Allows the capability to alter the lines of documents as well as creating and saving to documents
 * February 22, 2017
 */

package project1;

import java.io.*;
import java.util.*;

public class Documents {
	
	// CREATION OF DOUBLY LINKED LIST CLASS
	public static class DNode {
		private String data;
		DNode prev;
		DNode next;	
		public DNode() {
		}
		public DNode(String data)
		{
			this.data = data;
		}
	}
	
	// VARIOUS VARIABLE DECLARATION TO ENSURE SMOOTH FUNCTIONALITY
	private static DNode head = new DNode("");
	private static DNode last;
	private static DNode pos;
	private static int size;
	private static DNode tmpNode;
	private static DNode buffer = new DNode();
	private static DNode posLast;
	
	// RETURNS THE SIZE OF A STRING
	public static int stringSize() {
		size = 1;
		DNode t = head.next;
		while(t.next != null) {
			t = t.next;
			size++;
		}
		return size;
	}
	
	// RETURNS TRUE IS THE LIST IS EMPTY AND FALSE IF NOT
	public static boolean isEmpty() {
		if(head.next == null)
			return true;
		else
			return false;
	}

	// SETS THE DATA OF A NODE
	// USED AFTER EXITING THE LINE EDITOR
	public static void setData(String s, int lineNum) {
		DNode t = head.next;
		int count = 0;
		while(t.next != null && count < lineNum-1) {
			t = t.next;
			count++;
		}
		t.data = s;
	}
	
	// METHOD USED WHEN ADDING A NEW LINE TO AN EMPTY LIST
	public static void newLine(String line) {
		DNode t = new DNode(line);
		// IF THE LIST IS EMPTY, SET THE FIRST LINE 
		if(head.next == null) {
			head.next = t;
			last = t;
			}
		// ELSE ADD THE NODE TO THE END OF THE LIST
		else {
			last.next = t;
			t.prev = last;
			last = t;
		}
		size++;	
	}
	
	// METHOD USED TO LOAD A LIST FROM A FILE
	public static void newLineLoad(String line) {
		DNode t = new DNode(line);
		// IF THE LIST IS EMPTY, SET THE FIRST LINE
		if(head == null) {
			head = t;
			last = t;
			}
		// ELSE ADD THE LINE TO THE END OF THE LIST
		else {
			last.next = t;
			t.prev = last;
			last = t;
		}
		size++;	
	}
	
	//CREATES NEW NODES FOR THE COPY RANGE METHOD
	public static void newCopy(String line) {
		DNode PN = new DNode(line);
		if(buffer.next == null) {
			buffer.next = PN;
			posLast = PN;
		}
		
		else {
			posLast.next = PN;
			PN.prev = posLast;
			posLast = PN;
		}
	}
	
	// ALLOW THE USER TO CHOOSE WHERE TO INSERT MORE LINES
	public static void newLine2(String line, int linenumber) {
		DNode t = head.next;
		DNode newNode = new DNode(line);
		int k = 1;
		// IF INSERTING AT THE FIRST LINE
		if(linenumber == 0) {
			t.prev = newNode;
			newNode.next = t;
			head.next = newNode;
		}
		else {
			// LOOP TO THE POINT OF INSERTION
			while(t.next != null && k < linenumber) {
				t = t.next;
				k++;
			}
			// INSERTING AFTER THE FIRST LINE
			if(t.next != null) {
				newNode.next = t.next;
				t.next.prev = newNode;
				t.next = newNode;
				newNode.prev = t;
			}
			// INSERTING TO THE END OF THE LIST
			else {
				t.next = newNode;		
				newNode.prev = t;
			}
		}
	}
	
	// RETURN THE CONTENTS OF THE SPECIFIED LINE (NODE)
	public static String printData(String node) {
		return node;
	}

	// DISPLAY THE CONTENTS OF THE BUFFER/DOCUMENT
	public static void showAll() {
		try {
			DNode t = head.next;
			int count = 1;
			while(t != null) {
				System.out.println(count + ": " + t.data);
				t = t.next;
				count++;
			}
		}catch(NullPointerException e) {
			System.out.println("List Empty");
		}
	}
	
	// SELECT A LINE TO DISPLAY
	public static String showLine(String sLine) {
		DNode t = head.next;
		int target = Integer.parseInt(sLine);
		int count = 1;
		// IF THE LIST IS EMPTY
		if(t == null) {
			return "File Empty";
		}
		// IF THERE IS ONLY ONE NODE IN THE LIST
		else if(t.next == null) {
			return t.data;
		}
		// IF YOU ARE DISPLAYING THE FIRST LINE
		else if(target == 1) {
			return t.data;
		}
		// ELSE GO TO THE LINE THAT IS TO BE DISPLAYED
		else {
			while(count < target && t.next != null) {
				if(t.next != null) {
					t = t.next;
					count++;
				}
			}	
			return t.data;
		}
	}
	
	// COPY THE CONTENTS OF THE A RANGE OF NODES
	public static void copyRange(String from, String to) {
		DNode t = head.next;
		int Lfrom = Integer.parseInt(from);
		int Lto = Integer.parseInt(to);
		int i = 1;
		// GO TO THE POSITION WHERE THE COPYING BEGINS
		while(t != null && i < Lfrom) {
			t = t.next;
			i++;
		}
		// THEN COPY THE SERIES OF LINES UNTIL THE LAST POSITION OF COPYING
		while(t != null && i <= Lto) {
			newCopy(t.data);
			t = t.next;
			i++;
		}
	}
	
	// PASTE THE CONTENTS OF A SET OF COPIED NODES
	// INTO A POSITION IN THE DOCUMENTS NODES
	public static void pasteLines(int Pafter) {
		DNode p = buffer.next;
		while (p != null) {
			newLine2(p.data, Pafter);
			p = p.next;
			Pafter++;
		}
		
	}
	
	// DISPLAY THE DATA FROM EACH NODE IN A RANGE OF NODES
	public static void showRange(String from, String to) {
		DNode t = head.next;
		int Lfrom = Integer.parseInt(from);
		int Lto = Integer.parseInt(to);
		int i = 1;
		while(t != null && i < Lfrom) {
			t = t.next;
			i++;
		}
		while(t != null && i <= Lto) {
			System.out.println(i + ": " + t.data);
			t = t.next;
			i++;
		}
	}
	
	// DELETES A RANGE OF NODES SPECIFIED BY THE USER
	public static void deleteRange(String from, String to) {
		DNode t = head.next;
		pos = head.next;
		int Lfrom = Integer.parseInt(from);
		int Lto = Integer.parseInt(to);
		int i = 1;
		// COPIES THE CONTENTS OF THE DOCUMENT BEFORE 
		// THE POSITION LINES THAT ARE BEING DELETED
		while(t != null && i < Lfrom) {
			t = t.next;
			pos = pos.next;
			i++;
		}
		// DISPLAY THE LINES THAT ARE BEING DELETED
		while(t != null && i <= Lto) {
			System.out.println(i + ": " + pos.data);
			pos = pos.next;
			i++;
		}
		// MODIFY THE POINTERS BY DELETE THE SPECIFIED LINES
		if (pos == null && t.prev == null) {
			head.next = null;
		}
		else if(pos == null && t.prev != null) {
			t.prev.next = null;
		}
		else {
			t.prev.next = pos;
			pos.prev = t;
		}
		
	}
	
	// DELETE A LINE FROM THE FILE (LINKED LIST)
	public static void deleteLine(int target) {
		DNode t = head.next;
		int count = 1;
		// IF THE LIST IS EMPTY
		if(head == null) {
			System.out.println("File Empty.");
		}
		// IF THERE IS ONLY ONE NODE IN THE LIST
		else if(head.next == null) {
			head = null;
		}
		// IF YOU ARE DELETING THE FIRST LINE
		else if(target == 1) {
			head.next.prev = null;
			head = head.next;
			size--;
		}
		// ELSE GO TO THE LINE THAT IS TO BE DELETED
		else {
			// LOOP THAT WILL TRAVERSE TO THE DESIGNATED LINE (NODE)
			while(count < target) {
				if(t.next != null)
					t = t.next;
				count++;
			}	
			// DELETE THE LINE YOU ARE AT
			t.prev.next = t.next;
			if(t.next != null) {
				t.next.prev = t.prev;
				size--;
			}
		}	
	}
	
	// DELETE THE CONTENTS OF THE DLL
	public static void deleteAll() {
		head = null;
	}
	
	// PROMPTS THE USER TO SELECT A FILE TO OPEN
	public static void loadFile()
	{
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter the name of the File: ");
		String fileName = scan.nextLine();
		BufferedReader filereader = null;
		File file = new File(fileName);
		int tmp = 0;
		while(tmp < 1) {
			if(!file.exists()) {
				tmp++;
				System.out.println("File not found.");
			}
			else {
				try {
					filereader = new BufferedReader(new FileReader(file));
					Scanner fileScanner = new Scanner(new File("C:\\Users\\mvill\\CS310\\Proj1\\project1\\"+fileName));
					int tn = 1;
					String line2 = null;
					while((fileScanner.hasNextLine())) {
						if(tn == 1)
							line2 = fileScanner.nextLine();
						else if (tn > 1) {
							line2 = fileScanner.nextLine();
						}
						newLineLoad(line2);
						tn++;
					}
					showAll();
				} catch(IOException exception) {
					System.out.println("Could not open file.");
				} finally {
					try {
						filereader.close();
					} catch (IOException exception) {
						System.out.println("Could not close file.");
					}
				}
				tmp++;
			}
		}
	}
	
	// THIS FUNCTION WILL OPEN THE FILE NAMED BY THE USER
	// AND PROMPT FOR USER INPUT THAT WILL BE SAVED TO THE OPENED FILE
	public static void writeFile(String fileName) {
		FileWriter writer = null;
		BufferedWriter Bwriter = null;
		// WRITE TO AN EXISTING FILE, OR CREATE A NEW FILE TO WRITE THE CONTENTS OF THE BUFFER
		try {
			writer = new FileWriter("C:\\Users\\mvill\\CS310\\Proj1\\project1\\" + fileName);
			Bwriter = new BufferedWriter(writer);
			DNode t = head;
			while(t != null) {
				Bwriter.write(printData(t.data));
				Bwriter.newLine();
				t = t.next;
			}
		// THROW THIS EXCEPTION IF THERE IS AN ERROR WITH WRITING OR CREATING A FILE
		} catch(IOException exception) {
			System.out.println("Unable to write to file.");
		} finally { // LASTLY CLOSE THE WRITERS OPENED
			try {
				if(Bwriter != null)
					Bwriter.close();	
				if(writer != null)
					writer.close();
			} catch(IOException exception) {
				System.out.println("Unable to close file.");
			}
		}
	}
}
